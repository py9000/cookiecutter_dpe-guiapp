install_app:
  cmd.run:
    - name: >
        source /home/vagrant/dpe/dpeguiapp/miniconda3/bin/activate;
        source activate dpeguiapp;
        source env.sh;
        pip install -e .;
    - cwd: /vagrant
    - runas: vagrant
